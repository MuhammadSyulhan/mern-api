const express = require('express');
const { body } = require('express-validator');

const router = express.Router();

const blogController = require('../controllers/blog');

// [POST]: /v1/blog/post
router.post('/post', [
    body('title').isLength({ min: 5 }).withMessage('Title kurang dari 5 huruf'),
    body('body').isLength({ min: 5 }).withMessage('Body kurang dari 5 huruf')
], blogController.createBlogpost);

module.exports = router;