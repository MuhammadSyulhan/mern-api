const { validationResult } = require('express-validator');

exports.createBlogpost = (req, res, next) => {
    const title = req.body.title;
    // const image = req.body.image;
    const body = req.body.body;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        // console.log('err: ', errors);
        // res.status(400).json({
        //     message: 'Request error',
        //     data: null,
        // })
        const err = new Error('Input tidak sesuai');
        err.errorStatus = 400;
        err.data = errors.array();
        throw err;
    }

    const result = {
        message: 'Create Blog Post Success',
        data: {
            post_id: 1,
            title: title,
            // image: "image.png",
            body: body,
            created_at: "11/12/12",
            author: {
                uid: 1,
                name: "testing"
            }
        }
    }
    res.status(201).json(result);
}