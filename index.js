const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express()
const authRoutes = require('./src/routes/auth')
const blogRoutes = require('./src/routes/blog');

app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
})

app.use('/v1/auth', authRoutes);
app.use('/v1/blog', blogRoutes);

app.use((error, req, res, next) => {
    const status = error.errorStatus || 500;
    const message = error.message;
    const data = error.data;
    res.status(status).json({ message: message, data: data })
})

// DMpJGLiVUIrgNLrO
mongoose.connect('mongodb://sulhangop:DMpJGLiVUIrgNLrO@cluster0-shard-00-00.xawzp.mongodb.net:27017,cluster0-shard-00-01.xawzp.mongodb.net:27017,cluster0-shard-00-02.xawzp.mongodb.net:27017/<dbname>?ssl=true&replicaSet=atlas-jp3j75-shard-0&authSource=admin&retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        app.listen(4000, () => console.log('Connection success'));
    }).catch(err => console.log(err))